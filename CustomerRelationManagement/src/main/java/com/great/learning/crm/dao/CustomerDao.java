package com.great.learning.crm.dao;

import java.util.List;

import com.great.learning.crm.model.Customer;

public interface CustomerDao {
	public List<Customer> getCustomers();

	public void saveCustomer(Customer customer);

	public Customer getCustomer(int id);

	public void deleteCustomer(int id);
}
